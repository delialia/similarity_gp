function [] = plot_cqt(Xcq, xlen, fs)

c = Xcq.c;
B = Xcq.B;
fmin = Xcq.fmin;

%figure; imagesc(20*log10(abs(flipud(c))+eps));
figure; imagesc(abs(flipud(c))+eps);
%figure; imagesc(abs(c));
hop = xlen/size(c,2);
xtickVec = 0:round(fs/hop):size(c,2)-1;
set(gca,'XTick',xtickVec);
ytickVec = 0:B:size(c,1)-1;
set(gca,'YTick',ytickVec);
ytickLabel = round(fmin * 2.^( (size(c,1)-ytickVec)/B));
set(gca,'YTickLabel',ytickLabel);
xtickLabel = 0 : length(xtickVec) ;
set(gca,'XTickLabel',xtickLabel);
xlabel('time [s]', 'FontSize', 12, 'Interpreter','latex'); 
ylabel('frequency [Hz]', 'FontSize', 12, 'Interpreter','latex');
set(gca, 'FontSize', 10);


end