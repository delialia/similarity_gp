clear all;

% Input Synth Data C C# D D# E F F# G G# A A# B
filelist = dir('~/dev/similarity_GP/audio/*.aif');
data_path = '~/dev/similarity_GP/cqt_data/';

for i = 1 : length(filelist)
    input = filelist(i).name;
    % Read the input signal and convert to mono:
    [in_stereo, fs] = audioread(input);
    in_mono = mean(in_stereo, 2);
    in_mono=in_mono(1:12*fs);

    x = in_mono(:); 
    xlen = length(x);

    % CQT PARAMETERS
    fs = 44100;
    fmin = 27.5;
    B = 24; % 1/4 of a tone
    gamma = 20; 
    fmax = fs/2;

    % COMPUTE COEFFIENTS
    % full rasterized transform
     Xcq = cqt(x, B, fs, fmin, fmax, 'rasterize', 'full','gamma', gamma);

    c = Xcq.c;
    hop = xlen/size(c,2);

    % SAVE cqt data
    cqt_mat = abs(flipud(c));
    data_name = [data_path filelist(i).name(1:(end-4)) '.mat' ];
    save(data_name, 'cqt_mat');
    
    
end


data_list = dir('~/dev/similarity_GP/cqt_data/*.mat');

for j=1:length(data_list)
    
    load(data_list(j).name);
    figure, imagesc(cqt_mat);
    title(data_list(j).name);
    
end

% Make the bin with most energy first value

for k=1:length(data_list)
    
    load(data_list(k).name);
    [num, idx] = max(cqt_mat);
    
    for kk=1:size(cqt_mat,2)
        cqt_mat(:,kk) = circshift(cqt_mat(:,kk), length(cqt_mat(:,kk))-idx(kk), 1);
    end
    
    data_name = [data_path data_list(k).name(1:(end-4)) '_order.mat' ];
    save(data_name, 'cqt_mat');
    
    figure, imagesc(cqt_mat);
    title(data_list(k).name);
end

